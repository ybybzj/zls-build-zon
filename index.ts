import path from "node:path";

const port = process.env.PORT ?? 3030;
Bun.serve({
  port: port,
  async fetch(req) {
    const filePath = new URL(req.url).pathname;
    console.log(filePath);
    const file = Bun.file(path.join(import.meta.dir, filePath));
    return new Response(file);
  },
  error() {
    return new Response(null, { status: 404 });
  },
});
